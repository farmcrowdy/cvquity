<?php

namespace App;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class HttpHelper
{

    private $guzzle;
    private $grant_type;
    private $client_id;
    private $client_secret;
    private $username;
    private $password;
    private $base_uri;
    private $access_token;

    /**
    * HttpHelper constructor.
    */
    public function __construct()
    {
        
        $this->guzzle = new Client(['http_errors' => false, 'verify' => false]);
        // $this->guzzle = new Client();
        // $this->base_uri = 'https://api.crowdyvest.com/';
        // $this->base_uri = 'https://staging-api.crowdyvest.com/';
        $this->base_uri = config('app.base_url');
        $this->grant_type = getenv("grant_type");
        $this->client_id = getenv("client_id");
        $this->client_secret = getenv("client_secret");
    }


    /**
    * @param $endpoint
    * @param $array - Array of data to be JSON encoded
    * @return mixed
    */
    public function post($endpoint, $array) {
        $response = $this->guzzle->post($this->cleanEndpoint($this->base_uri.$endpoint), $array);
        $body = json_decode($response->getBody());
        return $body;
    }
    /**
    * @param $endpoint
    * @param int $page
    * @return mixed
    */
    public function get($endpoint, $array) {
        $response = $this->guzzle->request('GET',$this->cleanEndpoint($this->base_uri.$endpoint), $array);
        $body = json_decode($response->getBody());
        return $body;
    }
    /**
    * @param $endpoint
    * @param $array - Array of data to be JSON encoded
    * @return mixed
    */
    public function patch($endpoint, $array) {
        $response = $this->guzzle->patch($this->cleanEndpoint($this->base_uri.$endpoint), $array);
        $body = json_decode($response->getBody());
        return $body;
    }

    /**
    * @param $endpoint
    * @param $array - Array of data to be JSON encoded
    * @return mixed
    */
    public function put($endpoint, $array) {
        $response = $this->guzzle->put($this->cleanEndpoint($this->base_uri.$endpoint), $array);
        $body = json_decode($response->getBody());
        return $body;
    }

    /**
    * @param $endpoint
    * @return mixed
    */
    public function delete($endpoint, $array) {
        $response = $this->guzzle->delete($this->cleanEndpoint($this->base_uri.$endpoint), $array);
        $body = json_decode($response->getBody());
        return $body;
    }
    /**
    * Remove leading or trailing forward slashes from the endpoint.
    * @param $endpoint
    * @return string
    */
    private function cleanEndpoint($endpoint) {
        $endpoint = ltrim($endpoint,"/");
        $endpoint = rtrim($endpoint,"/");
        return $endpoint;
    }
}
