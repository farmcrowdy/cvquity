<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Carbon\Carbon;

class UserController extends Controller
{
    public function dashboard()
    {
        return view('user.dashboard');
    }

    public function join_waiting_list()
    {
        try {
            $result = postClientEndpoint('join-waiting-list', [
            ]); 
            if($result->success) {
                $notification = array(
                    'message' => $result->message,
                    'alert-type' => 'success'
                );
            } else {
                $notification = array(
                    'message' => $result->message,
                    'alert-type' => 'error'
                );
            }
            return back()->with($notification);
        } catch (\Throwable $th) {
            $notification = array(
                'message' => 'Unable to complete action',
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    
}
