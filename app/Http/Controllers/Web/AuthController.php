<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
// use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Providers\RouteServiceProvider;
use App\HttpHelper;
use GuzzleHttp\Client;
use Session;
use Socialite;


class AuthController extends Controller
{
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        // dd(Session::all());
        return view('auth.register');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $data['password_confirmation'] = $data['password'];
        $validator =  Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'phone_number' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'password_confirmation' => ['required'],
        ]);

        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'phone' => $data['phone_number'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function register(Request $request)
    {
        $client = new HttpHelper();
        $register = $client->post('register', [
            'form_params' => [
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'phone' => $request->phone_number,
                    'email' => $request->email,
                    'password' => $request->password,
                    'referral_code' => $request->referral_code,
            ]
        ]);

        if($register->success) {
            $request->session()->put('authenticated', $register->data->access_token);
            return redirect()->intended('/dashboard');
        } else {
            // dd($register);
            $errorMsg = $register->error;
            Session::put('errorMsg', $errorMsg);
            return redirect()->route('show.reg_form', compact('errorMsg'));;
        }
        
    }
    
    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    
    public function login(Request $request)
    {
        $client = new HttpHelper();
        $resultToken = $client->post('login', [
            'headers' => ['Content' => 'Application/json'],
            'form_params' => [ 
                'email' => $request->email,
                'password' => $request->password,
            ],
        ]);
        $request->session()->forget('authenticated');
        Session::flush();
        if($resultToken->success) {
            $req = $request->session()->put('authenticated', $resultToken->data->access_token);

            // get the user details and store it in the cache
            $getUserDetails = getClientEndpoint('user/profile');
            $user = $getUserDetails->data;
            return redirect()->intended('dashboard');
        } else {
            $errorMsg = $resultToken->message;
            Session::put('errorMsg', $errorMsg);
            $notification = array(
                'message' => 'Login',
                'alert-type' => 'error'
            );
            return redirect()->route('loginForm')->with($notification);
        }
    }

    public function logout()
    {
        $client = new HttpHelper();
        $getLogout = $client->post('logout',[]);
        Session::forget('authenticated');
        return redirect()->route('loginForm');
    }

    /**
     * Show the forgot password form
     */
    public function show_forgot_password_form()
    {
        return view('auth.passwords.forgot_password');
    }

    /**
     * Show the passord reset form after clicking the email
     */
    public function show_password_reset_form(Request $request, $token=NULL)
    {
        $token = request()->get('token');
        return view('auth.passwords.reset')->with(['token' => $token]);
    }

    public function change_password(Request $request) {
        $token = $request->token;
        $email = $request->email;
        $password = $request->password;
        $password_confirmation = $request->password_confirmation;

        $result = guestPostClientEndpoint('password/reset', [
            'token' => $token,
            'email' => $email,
            'password' => $password,
            'password_confirmation' => $password_confirmation,
        ]);
    
        if( isset($result->errors))
        {
            $errorMsg = $result->errors;
            $errorMsg = get_object_vars($errorMsg);
            
            Session::put('validationErrors', $errorMsg);
            return redirect()->back();
        }

        if($result->success) {
            $successMsg = $result->message;
            Session::put('success', $successMsg);
            // Show flash notification
            $notification = array(
                'message' => $result->message,
                'alert-type' => 'success'
            );
            return redirect()->back()->with([
                                            'notification' => $notification
                                            ]);
        } else {
            $errorMsg = $result->message;
            Session::put('errorMsg', $errorMsg);

            $notification = array(
                'message' => $result->message,
                'alert-type' => 'error'
            );
            return redirect()->back()->with([
                                            'notification' => $notification
                                            ]);
        }
        
    }

    public function send_password_reset_link(Request $request)
    {
        $email = $request->email;
        $result = guestPostClientEndpoint('password/email', [
            'email' => $email
        ]);
        dd($result);
        if($result->success) {
            $successMsg = $result->message;
            Session::put('success', $successMsg);
            // Show flash notification
            $notification = array(
                'message' => $result->message,
                'alert-type' => 'success'
            );
            return redirect()->back()->with([
                                            'notification' => $notification
                                            ]);
        } else {
            $errorMsg = $result->message;
            Session::put('errorMsg', $errorMsg);

            $notification = array(
                'message' => $result->message,
                'alert-type' => 'error'
            );
            return redirect()->back->with([
                                            'notification' => $notification
                                            ]);
        }
    }

    /**
     * Redirect the user to the Social Provider authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from Social Provider.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->stateless()->user();
        // dd($user);
    
        try {
                $provider = $provider;
                $username = $user->nickname;
                $name = $user->name;
                $email = $user->email;
                $imageUrl = $user->avatar;

                $result = guestPostClientEndpoint('social-login', [
                    'provider' => $provider,
                    'username' => $username,
                    'name' => $name,
                    'email' => $email,
                    'imageUrl' => $imageUrl
                ]);
                if($result->success) {
                    $req = session()->put('authenticated', $result->data);
                    // get the user details and store it in the cache
                    $getUserDetails = getClientEndpoint('user/profile');
                    $user = $getUserDetails->data;
        
                    // Cache::forever('user', $user);
                    // Cache::forever('getUserDetails', $getUserDetails);
                    return redirect()->intended('dashboard');
                } else {
                    $errorMsg = $result->message;
                    Session::put('errorMsg', $errorMsg);
                    return redirect()->route('loginForm')->with($errorMsg);
                }

        } catch (\Throwable $th) {
            throw $th;
        }

        // $user->token;
    }
}
