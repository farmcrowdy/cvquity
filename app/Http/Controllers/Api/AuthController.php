<?php

namespace App\Http\Controllers\Api;

use Sheets;
use App\User;
use Exceptions;
use Validator;
use App\Notifications\Welcome;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{
    /**
     * Register a new user
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        // validate form input data
        $validation = Validator::make($request->all(), [
            'first_name'    => 'required|string',
            'last_name'     => 'required|string',
            'email'         => 'required|email|unique:users',
            'password'      => 'required',
            'phone'         => 'required|numeric'
        ]);

        if ($validation->fails())
            return $this->errorResponse(
                'Failed validation.',
                $validation->errors(),
                422
            );

        // create new user account and assign wallet
        try {

            $user = User::create([
                'first_name'    => ucfirst($request->first_name),
                'last_name'     => ucfirst($request->last_name),
                'email'         => $request->email,
                'password'      => bcrypt($request->password),
                'phone'         => $request->phone
            ]);

            if ($user) {

                // write user details to google sheet
                $this->writeUserToSheets($user);

                // create customer account on paystack
                $customer = $this->createCustomer($user);

                $dedicatedAccount = $this->createDedicatedAccount($customer);

                $this->createWallet($user, $customer, $dedicatedAccount);

                // generate access token for user
                $token = $user->createToken('CVV2')->accessToken;

                // notify user of successful registration
                $user->notify(new Welcome($user));

                $data = ['access_token' => $token];

                return $this->successResponse('New account created successfully.', $data, 201);
            }
        } catch (Exceptions $e) {

            return $this->serverError();
        }
    }

    /**
     * Authenticate user
     * Assign access token
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        // validate input data
        $validation = Validator::make($request->all(), [
            'email'     => 'required|email',
            'password'  => 'required|string'
        ]);

        if ($validation->fails()) return $this->errorResponse(
            'Failed validation.',
            $validation->errors(),
            422
        );

        // try authenticating user
        try {

            $credentials = [
                'email'     => $request->email,
                'password'  => $request->password
            ];

            if (auth()->attempt($credentials)) {

                // get authenticated user
                $user = auth()->user();

                // assign access token to authenticated user
                $token = $user->createToken('CVV2')->accessToken;

                $data = ['access_token' => $token];

                return $this->successResponse('Login successful.', $data);
            } else {

                return $this->errorResponse('Username/Password do not match.');
            }
        } catch (Exception $ex) {

            return $this->serverError();
        }
    }

    /** 
     * Log a user out
     * Revoke access token
     * 
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        // revoke token granted to authenticated user
        try {

            auth()->user()->token()->revoke();

            return $this->successResponse('Logout successful.');
        } catch (Exception $ex) {

            return $this->serverError();
        }
    }

    /**
     * Write new user details to google sheet
     * 
     * @param $user
     * @return void
     */
    public function writeUserToSheets($user)
    {
        Sheets::spreadsheet('1OXjkL7fgJz7dcHU-7kWdKju_uQsBpddn2KHYShp8d0M')
            ->sheet('Cvquity Registrations')
            ->append([
                [$user->id, $user->email, $user->first_name, $user->last_name, $user->created_at->toDateTimeString()]
            ]);
    }
}
