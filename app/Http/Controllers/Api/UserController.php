<?php

namespace App\Http\Controllers\Api;

use Exception;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Retrieve user profile
     * 
     * @return \Illuminate\Http\Response
     */
    public function getProfile()
    {
        try {

            // get authenticate user
            $user = auth()->user();

            return $this->successResponse(
                'Retrieved profile successfully.',
                $user
            );
        } catch (Exception $e) {

            return $this->serverError();
        }
    }
}
