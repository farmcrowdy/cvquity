<?php

namespace App\Http\Controllers\Api;

use Exception;
use App\Wallet;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Log;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // format response for successful operations
    public function successResponse($message = 'Successful operation', $data = null, $code = 200)
    {
        if (is_null($data)) {

            return response()->json([
                'success'   => true,
                'message'   => $message,
            ], $code);
        } else {

            return response()->json([
                'success'   => true,
                'message'   => $message,
                'data'      => $data
            ], $code);
        }
    }

    // format response for unsuccessful operations
    public function errorResponse($message, $error = null, $code = 400)
    {
        if (is_null($error)) {

            return response()->json([
                'success'   => false,
                'message'   => $message
            ], $code);
        } else {

            return response()->json([
                'success'   => false,
                'message'   => $message,
                'error'     => $error
            ], $code);
        }
    }

    // format response if serve error occurs
    public function serverError()
    {
        return response()->json([
            'success'   => false,
            'message'   => 'Ops! Something went wrong. Please try again later.'
        ], 500);
    }

    /**
     * Create paystack customer
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function createCustomer($user)
    {
        // prepare data
        $email = $user->email;
        $first_name = $user->first_name;
        $last_name = $user->last_name;

        $secretKey = config('paystack.secretKey');

        $url = "https://api.paystack.co/customer";

        $fields = [
            'email' => $email,
            'first_name' => $first_name,
            'last_name' => $last_name
        ];

        $fields_string = http_build_query($fields);

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Authorization: Bearer " . $secretKey,
            "Cache-Control: no-cache",
        ));

        // returns the contents of the cURL
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //execute post
        $response = curl_exec($ch);
        $response = json_decode($response, true);

        if (!$response['status']) {

            dd($response);
        };

        $customer = $response['data'];

        return $customer;
    }

    /**
     * Create deidcated account for user
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function createDedicatedAccount($customer)
    {
        $secretKey = config('paystack.secretKey');

        $url = "https://api.paystack.co/dedicated_account";

        $fields = [
            'customer' => $customer['id'],
        ];

        $fields_string = http_build_query($fields);

        // open connection
        $ch = curl_init();

        // set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Authorization: Bearer " . $secretKey,
            "user-agent: Paystack-Developers-Hub",
            "Cache-Control: no-cache",
        ));

        // returns the contents of the cURL
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //execute post
        $response = curl_exec($ch);
        $response = json_decode($response, true);

        if (!$response['status']) {

            dd($response);
        };

        $dedicatedAccount = $response['data'];

        return $dedicatedAccount;
    }

    /**
     * Create user wallet
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function createWallet($user, $customer, $dedicatedAccount)
    {
        try {

            $bank_name = $dedicatedAccount['bank']['name'];

            Wallet::create([
                'user_id'        => $user->id,
                'customer_id'    => $customer['id'],
                'customer_code'  => $customer['customer_code'],
                'account_name'   => $dedicatedAccount['account_name'],
                'account_number' => $dedicatedAccount['account_number'],
                'currency'       => $dedicatedAccount['currency'],
                'bank_name'      => $bank_name
            ]);
        } catch (Exception $ex) {

            return $this->serverError();
        }
    }
}
