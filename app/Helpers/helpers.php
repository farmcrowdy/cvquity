<?php
// use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use App\HttpHelper;
use Carbon\Carbon;

/**
 * get the current user which was stored in the cache
 */
function getUser()
{
    $client = new HttpHelper();
    $token = Session::get('authenticated');
    
    $result = $client->get('user/profile', [
        'headers' => [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$token
        ]
    ]);
    $user = $result->data->user;
    return $user;
    // return Cache::get('user');
}

function getProfile()
{
    $client = new HttpHelper();
    $token = Session::get('authenticated');
    
    $result = $client->get('user/profile', [
        'headers' => [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$token
        ]
    ]);
    $user = $result->data;
    return $user;
}

// function getUserDetails()
// {
//     return Cache::get('getUserDetails');
// }

/**
 * get the token which was saved in the session
 */
function getToken()
{
    return Session::get('authenticated');
}

function getClientEndpoint($endpoint)
{
    $client = new HttpHelper();
    $token = Session::get('authenticated');
    
    $result = $client->get($endpoint, [
        'headers' => [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$token
        ]
    ]);

    return $result;
}

function postClientEndpoint($endpoint, $array)
{
    $client = new HttpHelper();
    $token = Session::get('authenticated');
    // dd($array);
    $result = $client->post($endpoint, 
                [
                    'headers' => [
                        'Accept' => 'Application/json',
                        'Authorization' => 'Bearer '.$token    
                    ],
                    'form_params' => $array
                ]);
    
    return $result;
}

function postClientImageEndpoint($endpoint, $array)
{
    $client = new HttpHelper();
    $token = Session::get('authenticated');
    // dd($array);
    $result = $client->post($endpoint, 
                [
                    'headers' => [
                        "Accept" => "multipart/form-data"],
                        "multipart" => [
                        "name" => "image",
                        // "contents" => file_get_contents($image),
                        // "filename" => $image->getClientOriginalName(),
                        'Authorization' => 'Bearer '.$token    
                    ],
                    'form_params' => $array
                ]);
    
    return $result;
}

function guestPostClientEndpoint($endpoint, $array)
{
    $client = new HttpHelper();
    $result = $client->post($endpoint, [
        'headers' => [
            'Accept' => 'application/json',
        ],
        'form_params' => $array
    ]);
    
    return $result;
}

function guestGetClientEndpoint($endpoint)
{
    $client = new HttpHelper();
    $result = $client->get($endpoint, 
                [
                    'headers' => [
                        'Accept' => 'Application/json', 
                    ],
                    
                ]);
    
    return $result;
}

function patchClientEndpoint($endpoint, $array)
{
    $client = new HttpHelper();
    $token = Session::get('authenticated');
    // dd($array);
    $result = $client->patch($endpoint, [
        'headers' => [
            'Accept' => 'Application/json',
            'Authorization' => 'Bearer '.$token    
        ],
        'form_params' => $array
        
        ]);
    
    return $result;
}

function putClientEndpoint($endpoint, $array)
{
    $client = new HttpHelper();
    $token = Session::get('authenticated');
    $result = $client->put($endpoint, [
        'headers' => [
            'Accept' => 'Application/json',
            'Authorization' => 'Bearer '.$token    
        ],
        'form_params' => $array
        
        ]);
    return $result;
}

function deleteClientEndpoint($endpoint, $array)
{
    $client = new HttpHelper();
    $token = Session::get('authenticated');
    $result = $client->delete($endpoint, [
        'headers' => [
            'Accept' => 'Application/json',
            'Authorization' => 'Bearer '.$token    
        ],
        'form_params' => $array
        
        ]);
    return $result;
}


function getProducts(){
    // get the product id from api
    $getProduct = getClientEndpoint('products');
    $products = $getProduct;

    return $products;
}
/**
 * @param int $cycle
 * @param date $endDate
 */
function calculatePercentage($cycle, $startDate) {
    $today = Carbon::now();
    $parseStartDate = Carbon::parse($startDate);
    $diff = $today->diffInDays($parseStartDate);
    $res = $diff/$cycle * 100;
    $result = floor($res);
    if($result > 100) {
        return 100;
    }
    return $result;
}

function targetBalancePercentage($target, $targetBalance)
{
    $target == 0 ?  $result = 0 : $result = floor($targetBalance/$target * 100);
    return $result;
}

// calculate the accrued balance for each running savings(SaveVault, saveFlex)
function getVaultAccruedBalance($roi=0, $startDate=NULL, $duration=1, $savedAmount=0)
{
    $today = Carbon::now();
    $roiPercent = $roi/100;
    $amt = floatval(preg_replace('/[^\d.]/', '', $savedAmount));
    $parseStartDate = Carbon::parse($startDate);
    $diff =  $today->diffInMonths($parseStartDate);
    $getAccruedPercent = ($roiPercent * $diff)/$duration;
    // $savedAmount = intval($savedAmount);
    $getAccruedAmount = $getAccruedPercent * $amt;
    $result =  ($amt + $getAccruedAmount);
    return number_format($result, 2);
}

function fetchBankId($bankName=NULL)
{
    $getBanks = getClientEndpoint('banks');
    $banks = $getBanks->data;
    foreach($banks as $bank)
    {
        if(stripos($bank->name, $bankName) ){
            return $bank->id;
        } else {
            return null;
        }
    }
}

function calculateNextRound($dateOfNextRound)
{
    $today = Carbon::today();
    $nextRoundDate = Carbon::parse($dateOfNextRound);

    $diff = $today->diffInDays($nextRoundDate, false);
    $result = $diff <= 0 ? $result = 0 : $result = $diff;

    return $result;

}

