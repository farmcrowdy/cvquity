<?php

return [

    /**
     * --------------------------------------------------------------------------
     * Paystack Account Configuration
     * --------------------------------------------------------------------------
     * When making api calls to paystack server, bearer token is required for
     * authentication. Set the token here.
     */

    'secretKey' => env('PAYSTACK_SECRET_KEY')

];
