@extends('layouts.auth')
@section('content')
<section id="userboard">
        <div class="container">
            
            <div class="panel-wrapper">
                <div class="single-panel name-panel">
                    <img src="{{ asset('app/img/avatar.png')}}" alt="" srcset="">
                    <h2>Welcome, {{ getProfile()->last_name }}.</h2>
                    <p>{{ getProfile()->email }}</p>
                    <!-- <hr>
                    <p>12 Aug. 2020</p> -->

                </div>
                <div class="single-panel numbers-panel">
                    <h2>₦0.00</h2>
                    <p>Escrow Balance</p>
                    <br>
                    <hr>
                    <!-- <a class="panel-btn">Top-up Funds</a> -->
                </div>
                <div class="single-panel numbers-panel">
                    <h2>22 Sep 2020</h2>
                    <p>{{ calculateNextRound('22 Sep 2020') }} Days till next round.</p>
                    <br>
                    <hr>
                    
                    <a class="panel-btn" onclick="joinWaitingList()">
                            Join the waiting list
                    </a>
                </div>
            </div>
        </div>
        <!-- <form id="join-list" action="{{ route('join-waiting-list') }}" method="POST" style="display: none;">
            @csrf
        </form> -->
    </section>
@endsection