@extends('layouts.guest')

@section('content')
<div load-partial="partials/auth_header.html" class="header_sticky"></div>
    <section class="auth_container">
        <form action="{{ route('send.password.reset_link') }}" method="POST">
            @csrf
            <h4>Reset your password</h4>
            @if (Session::get('errorMsg'))
                <div class="alert alert-danger">
                    <ul>
                        <li>{{ Session::pull('errorMsg' )}}</li>
                    </ul>
                </div>
            @endif
            @if (Session::get('success'))
                <div class="alert alert-success">
                    <ul>
                        <li>{{ Session::pull('success' )}}</li>
                    </ul>
                </div>
            @endif
            
            <div class="form_group pass_relative">
                <input type="email" placeholder="Email" name="email" required>
                <span class="aboslute_span">Email</span>

            </div>
            <button class="create_acct">Send Reset Password</button>
            <p class="altp forgot_pass">Don’t have an account? <a href="{{ route('show.reg_form') }}">Create account</a></p>
        </form>
    </section>
@endsection