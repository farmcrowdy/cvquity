@extends('layouts.guest')

@section('content')                 
<section class="auth_container">
    <form action="{{ route('change.password') }}" method="POST">
        @csrf
        <input type="hidden" name="token" value="{{ $token }}">
        <h4>Create your new password</h4>
        @if (Session::get('validationErrors'))
            <div class="alert alert-danger">
                <ul>
                
                    @foreach(Session::pull('validationErrors' ) as $key => $error)
                        @foreach($error as $err)
                        <li>{{ $err }}</li>
                        @endforeach
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::get('errorMsg'))
            <div class="alert alert-danger">
                <ul>
                    <li>{{ Session::pull('errorMsg' )}}</li>
                </ul>
            </div>
        @endif
        @if (Session::get('success'))
            <div class="alert alert-success">
                <ul>
                    <li>{{ Session::pull('success' )}}</li>
                </ul>
            </div>
        @endif

        <div class="form_group pass_relative">
            <input type="email" name="email"  placeholder="Email">
            <span class="aboslute_span">Email</span>

        </div> 
        <div class="form_group pass_relative">
            <input type="password" name="password" id="password_input" placeholder="Password">
            <span class="aboslute_span">Password</span>

            <span class="span_absolute hidden_span span_password">
                <img src="{{asset('img/icon/eye.svg')}}" class=" " alt="see password">
                <img src=" {{asset('img/icon/eye_off.svg')}}" class="eye_off" alt="see password">
            </span>
        </div> 
        
        <div class="form_group pass_relative">
            <input type="password" name="password_confirmation" class="password" placeholder="Confirm Password">
            <span class="aboslute_span">Confirm Password</span>

            <!-- <span class="span_absolute hidden_span_confirm span_absolute_confirm">
                <img src="{{asset('img/icon/eye.svg')}}" class=" " alt="see password">
                <img src=" {{asset('img/icon/eye_off.svg')}}" class="eye_off" alt="see password">
            </span> -->
        </div> 

        <button class="create_acct">Reset Password</button>

    </form>
</section>
@endsection
