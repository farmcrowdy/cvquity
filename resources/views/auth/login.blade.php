@extends('layouts.guest')

@section('content')
    <section class="auth_container">
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <h4>Sign into CVQuity</h4>
            @if (Session::get('errorMsg'))
                <div class="alert alert-danger">
                    <ul>
                        <li>{{ Session::pull('errorMsg' )}}</li>
                    </ul>
                </div>
            @endif
            <div class="form_group pass_relative">
                <input class="@error('email') is-invalid @enderror" type="email" placeholder="Email Address" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                @error('email')
                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                @enderror
                <span class="aboslute_span">{{ __('E-Mail Address') }}</span>
            </div>

            <div class="form_group pass_relative">
                <input type="password" name="password" id="password_input" class="@error('password') is-invalid @enderror"  required autocomplete="current-password"  placeholder="Password">
                <span class="aboslute_span">Password</span>
                @error('password')
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <span class="span_absolute hidden_span">
                    <img src="{{ asset('img/icon/eye.svg') }}" class=" " alt="see password">
                    <img src="{{ asset('img/icon/eye_off.svg') }}" class="eye_off" alt="see password">
                </span>
            </div>

            <p class="forgot_pass">
                <a href="{{ url('forgot/passwd') }}">{{ __('Forgot Your Password?') }}</a>
            </p>
            <button class="create_acct">Sign into your account</button>
            <p class="altp">Don’t have an account? <a href="{{ route('register') }}">Create account</a></p>
            <p class="or">OR</p>
            <p class="sign_options">Sign up with</p>
            <div class="signup_socials">
                <a href="{{ url('login/facebook') }}">
                    <button type="button">
                        <svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect width="36" height="36" rx="18" fill="#4267B2"/>
                        <path d="M19.202 29.9999V19.0523H22.8766L23.4268 14.7859H19.202V12.0619C19.202 10.8267 19.545 9.98482 21.3165 9.98482L23.5757 9.98377V6.16793C23.1848 6.11613 21.8438 6 20.2836 6C17.0263 6 14.7962 7.98818 14.7962 11.6396V14.786H11.1121V19.0524H14.7961V30L19.202 29.9999Z" fill="white"/>
                    </svg>
                    </button>
                </a>
                <a href="{{ url('login/google') }}">
                    <button type="button">
                        <svg width="37" height="36" viewBox="0 0 37 36" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect x="0.28418" width="36.1826" height="36" rx="18" fill="#EA4335"/>
                            <path d="M6.32226 18.0156C6.0812 11.7009 11.6404 5.86533 17.9935 5.78962C21.2316 5.51484 24.3818 6.76593 26.8323 8.80642C25.8271 9.90608 24.8043 10.993 23.7119 12.0106C21.5546 10.7066 18.9572 9.71283 16.436 10.5954C12.3694 11.7475 9.90671 16.5247 11.4116 20.4829C12.6578 24.6168 17.7115 26.8855 21.6722 25.1489C23.723 24.4183 25.0752 22.5355 25.6685 20.5125C23.3181 20.4659 20.9671 20.495 18.6167 20.4305C18.6108 19.0391 18.605 17.653 18.6108 16.2616C22.5305 16.2558 26.4561 16.2441 30.3816 16.279C30.6227 19.6882 30.1172 23.3366 27.8896 26.0728C24.8395 29.9785 19.2042 31.1248 14.6198 29.5926C9.75452 28.0026 6.21636 23.1259 6.32226 18.0156Z" fill="white"/>
                        </svg>
                    </button>
                </a>
                <!-- <a href="{{ url('login/linkedin') }}">
                    <button type="button">
                        <svg width="37" height="36" viewBox="0 0 37 36" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0)">
                                <path d="M18.7334 0C8.7974 0 0.733398 8.064 0.733398 18C0.733398 27.936 8.7974 36 18.7334 36C28.6694 36 36.7334 27.936 36.7334 18C36.7334 8.064 28.6694 0 18.7334 0Z" fill="#0077B5"/>
                                <path d="M28.7272 26.4937C27.5459 26.4656 26.3928 26.4656 25.2115 26.4937C24.9584 26.4937 24.9022 26.4374 24.9022 26.1843C24.9022 24.1031 24.9022 21.9937 24.9022 19.9124C24.9022 19.4343 24.874 18.9562 24.7334 18.5062C24.3115 17.0437 22.624 16.5093 21.4147 17.4937C20.7678 17.9999 20.5147 18.7031 20.5147 19.5468C20.5147 21.5156 20.5147 23.4843 20.5147 25.4531C20.5147 25.7062 20.4865 25.9593 20.5147 26.2406C20.5428 26.4656 20.4303 26.5218 20.2334 26.4937C19.024 26.4937 17.8428 26.4937 16.6334 26.4937C16.4084 26.4937 16.3522 26.4374 16.3522 26.2124C16.3803 24.4406 16.3803 22.6687 16.3803 20.8687C16.3803 18.6749 16.3803 16.4812 16.3522 14.3156C16.3522 14.0624 16.4084 14.0062 16.6334 14.0062C17.8428 14.0062 19.024 14.0062 20.2334 14.0062C20.4584 14.0062 20.5147 14.0624 20.5147 14.2874C20.5147 14.7374 20.5147 15.1874 20.5147 15.7218C20.599 15.6374 20.6272 15.6093 20.6553 15.5812C21.7522 13.9781 23.299 13.5281 25.1272 13.8093C27.2365 14.1468 28.5865 15.6093 28.924 17.8593C29.0084 18.3937 29.0365 18.9281 29.0365 19.4624C29.0365 21.7124 29.0365 23.9343 29.0365 26.1843C29.0365 26.4093 28.9803 26.4937 28.7272 26.4937Z" fill="white"/>
                                <path d="M14.1021 20.2501C14.1021 22.2188 14.1021 24.1876 14.1021 26.1563C14.1021 26.4095 14.0459 26.4938 13.7928 26.4938C12.6115 26.4657 11.4303 26.4938 10.249 26.4938C10.024 26.4938 9.96777 26.4376 9.96777 26.2126C9.96777 22.247 9.96777 18.2532 9.96777 14.2876C9.96777 14.0907 10.024 14.0063 10.249 14.0063C11.4584 14.0063 12.6678 14.0063 13.8771 14.0063C14.1303 14.0063 14.1584 14.0907 14.1584 14.3157C14.1021 16.2845 14.1021 18.2532 14.1021 20.2501Z" fill="white"/>
                                <path d="M14.299 10.7439C14.0177 11.8689 12.8646 12.5158 11.5427 12.2908C10.1365 12.0658 9.34898 10.6876 9.85523 9.33764C10.1927 8.49389 11.0084 7.98764 12.049 8.01576C13.624 7.98764 14.6646 9.25326 14.299 10.7439Z" fill="white"/>
                            </g>
                            <defs>
                                <clipPath id="clip0">
                                    <rect x="0.733398" width="36" height="36" fill="white"/>
                                </clipPath>
                            </defs>
                        </svg>

                    </button>
                </a> -->
            </div>
        </form>
    </section>
@endsection
