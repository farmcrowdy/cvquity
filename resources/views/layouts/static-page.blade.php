<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#171C26">
    <link rel="stylesheet" href="{{ asset('css/cvmeister.css')}}">
    <link rel="stylesheet" href="{{ asset('css/nav2.css')}}">
    <link rel="stylesheet" href="{{ asset('fonts/reader/stylesheet.css')}}">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="{{ asset('js/nav.js')}}"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.9.0/css/bulma.css">
    <title>CVQuity</title>
</head>
<body>
  <div class="loki-wrapper">
    <div class="loki-header loki-header-transparent-light">
      <div class="loki-container">
        <div class="loki-logo" id="w-logo">
          <a href="{{ route('home') }}">
            <img src="{{ asset('img/logo_white.svg')}}" alt="CVQuity">
          </a>

        </div>
        <div class="loki-logo" id="m-logo">
          <a href="#">
            <img src="img/logo.svg"  alt="CVQuity">
          </a>
        </div>
        <input type="checkbox" id="loki-toggle" class="loki-toggle">
        <label for="loki-toggle"><span></span><span></span><span></span></label>
        <nav class="loki-menu loki-submenu-scale" role="navigation">
          <ul>
            <!-- <li class="">
              <a href="#">Startup? Raise funds</a>
            </li>
            <li>
              <a href="#">Contact</a>
            </li> -->

            <!-- render this navbar if authenticated -->
            @if(Session::get('authenticated'))
              <li class="nav-btn">
                <a href="{{ route('dashboard') }}">Dashboard</a>

              </li>
              <li class="nav-btn-inv">
                <a href="{{ route('show.reg_form') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
              </li>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
              </form>
            @else
            <li class="nav-btn-inv">
              <a href="{{ route('loginForm') }}">Log in</a>
              <!-- <a href="#">Log in</a> -->

            </li>
            <li class="nav-btn">
              <a href="{{ route('show.reg_form') }}">Sign up</a>
              <!-- <a href="#">Sign up</a> -->
            </li>
            @endif
          </ul>
        </nav>
      </div>
      <div class="loki-header-shadow"></div>
    </div>
    <div class="loki-header-spacer"></div>
  </div>

    @yield('content')


    <footer>
    <div class="columns is-centered bg-white">
      <div class="column is-6 is-narrow has-text-centered">
        <img data-aos="fade-up" src="../img/logo.svg" alt="" srcset="">
        <!-- <p class="powered-by">Powered by Crowdyvest</p> -->
        <div class="foot-contact" data-aos="fade-up" data-aos-easing="linear"
        data-aos-duration="1500">
          <p>2a Opeyemisi Bamidele Crescent,<br>
          Off Freedom Way, Lekki Phase 1, Lagos.</p>
          <p>   <a href="mailto:cvquity@crowdyvest.com">cvquity@crowdyvest.com</a></p>
          <p>+234 909 9999 830</p>
        </div>
      </div>
    </div>
    <div class="container ">

      <div class="columns is-centered">
        <div class="column is-12 is-narrow foot-legal" data-aos="fade-up" data-aos-easing="linear"
        data-aos-duration="1500">
          All text, graphics, audio files, code, downloadable material, and other works on this website are the copyrighted works of Crowdyvest. All Rights Reserved. Any unauthorized redistribution or reproduction of any copyrighted materials on this website is strictly prohibited. Other product and company names are trademarks of their respective owners. <br><br> Copyright © 2020 Crowdyvest. All rights reserved.
        </div>
      </div>

    </div>

  </footer>
    <!-- <script src="https://unpkg.com/stein-expedite@0.0.4/dist/index.js"></script> -->
    <script src="{{ asset('js/nav.js')}}"></script>
    <script>
      AOS.init();
    </script>
    <script src="https://unpkg.com/stein-expedite@0.0.4/dist/index.js"></script>
</body>
</html>
