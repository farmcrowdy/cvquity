<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.9.0/css/bulma.css">
    <link rel="stylesheet" href="{{ asset('app/css/cvquity.css')}}">
    <link rel="stylesheet" href="{{ asset('app/css/nav.css')}}">
    <link rel="stylesheet" href="{{ asset('app/fonts/reader/stylesheet.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css">
    <title>CVQuity</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>
</head>
<body>
    <!-- Feed these details to the JS files -->
    <script>
        var baseUrl = "{{ url('/') }}";
        var token = "{{ Session::get('authenticated') }}"
        // let public_key = "{{ config('app.paystack_key')  }}"
        let endpoint = "{{ config('app.base_url') }}"
    </script>
    <div class="loki-wrapper">
        <div class="loki-header">
            <div class="loki-container">
                <div class="loki-logo">
                    <a href="{{ route('home') }}">
                        <img src="{{ asset('app/img/logo.svg')}}" alt="Loki">
                    </a>
                </div>
                <input type="checkbox" id="loki-toggle" class="loki-toggle">
                <label for="loki-toggle"><span></span><span></span><span></span></label>
                <nav class="loki-menu loki-submenu-scale" role="navigation">
                    <ul>
                        <!-- class="loki-menu-current" -->
                        <li>
                            <a href="{{ route('dashboard') }}">Dashboard</a>
                        </li>
                        <li>
                            <a href="#cvq-how">How it Works</a>
                        </li>
                        <li>
                            <a href="#cvq-faq">FAQs</a>
                        </li>
                        <li>
                            <a href="{{ route('about') }}">About</a>
                        </li>
                        @if(Session::get('authenticated'))
                            <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
                            </li>
                        @endif
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                        <!-- <li class="loki-submenu-right">
                        <a href="#">Contact</a>
                        </li> -->
                    </ul>
                </nav>
            </div>
            <div class="loki-header-shadow"></div>
        </div>
        <div class="loki-header-spacer"></div>
    </div>

    @yield('content')
    <section id="cvq-how">
        <div class="container">
            <div class="columns">
                <div class="column is-4 cvq-side-img">
                    <!-- <img src="{{ asset('app/img/how_img.svg')}}" alt="" srcset=""> -->
                    <img src="{{ asset('app/img/how_img.svg')}}" alt="" srcset="">
                </div>
                <div class="column is-1"></div>
                <div class="column is-7 cvq-side-text" >
                    <h2>How CVQuity works.</h2>
                    <div data-stein-url="https://api.steinhq.com/v1/storages/5f2304ec5d3cdc44fcd7d26a/howitworks">
                        <div class="how-boxx">
                            <h3>@{{title}}</h3>
                            <p>@{{body}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Docs section for startups  -->
    <section id="cvq-docs">
      <div class="container">

          <div class="columns">
            <div class="column is-4 cvq-side-img">
              <img src="{{ asset('app/img/docs.svg')}}" alt="" srcset="">
            </div>
            <div class="column is-1"></div>
            <div class="column is-7 cvq-side-text" >
              <h2>Documents from Startups</h2>
              <div data-stein-url="https://api.steinhq.com/v1/storages/5f2304ec5d3cdc44fcd7d26a/documents">
                <div class="how-boxx shaded">
                  <p>@{{documents}}</p>
              </div>
              </div>

            </div>
          </div>
      </div>
    </section>
    <section id="cvq-faq">
        <div class="container">
            <div class="columns">
                <div class="column is-4 cvq-side-img">
                    <img src="{{ asset('app/img/faw_img.svg')}}" alt="" srcset="">
                </div>
                <div class="column is-1"></div>
                <div class="column is-7 cvq-side-text">
                    <h2>Frequently Asked Questions</h2>
                    <div class="faq-section" id="safety">
                    <h3 class="faq-section-title">General</h3>
                    <div class="faq-box"
                        data-stein-url="https://api.steinhq.com/v1/storages/5f2301db5d3cdc44fcd7d268/general">
                    <div>
                    <details>
                        <summary class="faq-question">@{{question}}</summary>
                        <p class="faq-answer">@{{response}}</p>
                    </details>
                </div>
            </div>
            <hr>
            <h3 class="faq-section-title">Investors</h3>
            <div class="faq-box"
                data-stein-url="https://api.steinhq.com/v1/storages/5f2301db5d3cdc44fcd7d268/investor">
            <div>
                <details>
                    <summary class="faq-question">@{{question}}</summary>
                    <p class="faq-answer">@{{response}}</p>
                </details>
            </div>
        </div>
        <hr>
        <h3 class="faq-section-title">Startups</h3>
        <div class="faq-box"
            data-stein-url="https://api.steinhq.com/v1/storages/5f2301db5d3cdc44fcd7d268/startup">
            <div>
                <details>
                    <summary class="faq-question">@{{question}}</summary>
                    <p class="faq-answer">@{{response}}</p>
                </details>
            </div>
        </div>
               <!-- </div>
            </div>
          </div>
        </div> -->
    </section>
    <footer>
    <div class="columns is-centered bg-white">
      <div class="column is-6 is-narrow has-text-centered">
        <img data-aos="fade-up" src="../img/logo.svg" alt="" srcset="">
        <!-- <p class="powered-by">Powered by Crowdyvest</p> -->
        <div class="foot-contact" data-aos="fade-up" data-aos-easing="linear"
        data-aos-duration="1500">
            <p>2nd Floor, SRO Plaza KM 11, Lekki Epe Expressway,<br> Ikate Elegushi, Lekki, Lagos.</p>
            <p><a href="mailto:cvquity@crowdyvest.com">cvquity@crowdyvest.com</a></p>
            <p>+234 909 0006 743</p>
        </div>
      </div>
    </div>
    <div class="container ">

      <div class="columns is-centered">
        <div class="column is-12 is-narrow foot-legal" data-aos="fade-up" data-aos-easing="linear"
        data-aos-duration="1500">
          All text, graphics, audio files, code, downloadable material, and other works on this website are the copyrighted works of Crowdyvest. All Rights Reserved. Any unauthorized redistribution or reproduction of any copyrighted materials on this website is strictly prohibited. Other product and company names are trademarks of their respective owners. <br><br> Copyright © 2020 Crowdyvest. All rights reserved.
        </div>
      </div>

    </div>

  </footer>

  <script src="https://unpkg.com/stein-expedite@0.0.4/dist/index.js"></script>
        <script src="{{ asset('js/custom.js')}}"></script>
    <script src="{{ asset('app/js/nav.js')}}"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>

    <script>
        @if(Session::has('message'))
            var type = "{{ Session::get('alert-type', 'info') }}";
            switch(type){
                case 'info':
                    toastr.info("{{ Session::get('message') }}");
                    break;

                case 'warning':
                    toastr.warning("{{ Session::get('message') }}");
                    break;

                case 'success':
                    toastr.success("{{ Session::get('message') }}");
                    break;

                case 'error':
                    toastr.error("{{ Session::get('message') }}");
                    break;
            }
        @endif
    </script>
</body>
</html>
