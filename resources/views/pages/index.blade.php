@extends('layouts.static-page')
@section('content')
    <section class="hero is-black  is-fullheight is-medium">
        <!-- Hero content: will be in the middle -->
        <div class="hero-body">
            <div class="container">
                <div class="columns">
                    <div class="column is-6 hero-text-side" data-stein-url="https://api.steinhq.com/v1/storages/5f2304ec5d3cdc44fcd7d26a/index">
                        <h1 class="hero-title">@{{title}}</h1>
                            <h2 class="hero-subtitle">@{{subtitle}}</h2>
                            <a href="{{ route('login') }}" class="hero-button">@{{cta}}</a>
                            
                    </div>
                    <!-- <div class="column is-1"></div> -->
                    <div class="column is-6 hero-side has-text-centered">
                        <img src="img/side-africa.svg" data-aos="fade-up" alt="" srcset="">
                    
                    </div>
                </div>
            </div>
        </div>
    
    </section>
@endsection