@extends('layouts.auth')
@section('content')
<section id="why">
        <div class="container">
            <div class="columns is-centered">
                <div class="column is-6 is-narrow has-text-centered">
                    <h2 class="section-title">
                        What is CVQuity?
                    </h2>
                    <p class="section-subtitle">CVQuity is Nigeria’s first equity crowdfunding platform. Founded by Crowdyvest, Cvquity allows individuals to invest in highly vetted startups that are solving global challenges across different sectors.</p>
                </div>
                
                
            </div>
            <div class="columns is-centered">
                <div class="column is-6 is-narrow sectors">
                    <div class="sector-wrapper">
                      <div class="sector-box">
                        <h3>Real Estate</h3>
                      </div>
                      <div class="sector-box">
                        <h3>Agriculture</h3>
                      </div>
                      <div class="sector-box">
                        <h3>Transportation</h3>
                      </div>
                      <div class="sector-box">
                        <h3>Healthcare</h3>
                      </div>
                      <div class="sector-box">
                        <h3>Environment</h3>
                      </div>
                      <div class="sector-box">
                        <h3>Energy</h3>
                      </div>
                      <div class="sector-box">
                        <h3>Education</h3>
                      </div>
                      <div class="sector-box">
                        <h3>Media</h3>
                      </div>
                      
                    </div>
                  </div>
            </div>

           
        </div>
    </section>
    <section id="portfolio">
      <div class="container">
        <div class="columns this-port">
          <div class="column is-4">
            <h2 class="section-title">
             Startups in our portfolio
          </h2>
          <!-- <p class="section-subtitle">CVQuity is Nigeria’s first equity crowdfunding platform. Founded by Crowdyvest.</p> -->
      
          </div>
          <div class="column is-1"></div>
          <div class="column is-7">
            <div class="portfolio-wrapper"> 
              <div class="portfolio-box with-logo has-text-centered">
                <img src="../img/plentywaka_logo.svg" alt="" srcset="">
                
              </div>
              <div class="portfolio-box has-text-centered">
                <!-- <img src="img/lock.svg" alt="" class="lockicon"> -->
                <a href="app/login.html">
                  <h3>
                    To be announced <br>13th October 2020
                  </h3>
                </a>
              </div>
              <div class="portfolio-box has-text-centered">
                <!-- <img src="img/lock.svg" alt="" class="lockicon"> -->
                <a href="app/login.html">
                  <h3>
                    To be announced <br>13th October 2020
                  </h3>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="benefits2">
        <div class="container">
            <div class="columns">
                <div class="column is-6">
                    <div class="ver-benefits">
                        <h3>Diversify your portfolio</h3>
                    <p>Get your skin in the game with Cvquity. Our minimum funding requirement starts at N2 Million, an amount 100x lower than the average funding round for startup investments, giving you the opportunity to easily diversify your portfolio across different startups.</p>
                    </div>
                    <div class="ver-benefits">
                        <h3>Strict Due Diligence</h3>
                <p>We sift through hundreds of applications to select the top 1% startups. The selected startups go through a strict due diligence process that involves our risk and compliance department. Furthermore, each startup is required to submit a list of documents that you will be able to access as a potential funder of the business.</p>
                    </div>
                    <div class="ver-benefits">
                        <h3>Power the Future</h3>
                        <p>Startups are the future. They are made up of individuals who push the limits, overcome insurmountable challenges, and beat the odds. By lending your support, you will be part of those building the future of this nation and empowering the next generation of startups.</p>
                             </div>
                </div>
                <div class="column is-6 ver-image-side has-text-centered">
                    <img src="../img/side-img.jpg" alt="" srcset="">
                </div>
            </div>
        </div>
    </section>
    
    <!-- <section id="cta">
      <div class="container">
          <div class="columns is-centered">
              
              <div class="column is-9 is-narrow has-text-centered">
                <div class="cta-box" data-aos="fade-up" data-aos-easing="linear"
                data-aos-duration="1500">
                  <h2 class="cta-title">
                    We’re building a simple, safer and more profitable place for new investors to fund the future.
                </h2>
                <a href="#" class="cta-button">Sign up today</a>
                </div>

            </div>
          </div>
          
      </div>
  </section> -->
@endsection