<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::prefix('v1')->group(function () {

    Route::post('register', 'Api\AuthController@register'); // register new user account

    Route::post('login', 'Api\AuthController@login'); // authenticate a user

    Route::middleware('auth:api')->group(function () {

        Route::post('logout', 'Api\AuthController@logout'); // log authenticated user out

        Route::get('user/profile', 'Api\UserController@getProfile'); // retrieve authenticated user profile

    });
});
