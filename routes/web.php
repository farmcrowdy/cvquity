<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Web\PagesController@index')->name('home');

// Auth
Route::get('login', ['uses' => 'Web\AuthController@showLoginForm', 'as' => 'loginForm']);
Route::post('login', 'Web\AuthController@login')->name('login');
Route::get('register', 'Web\AuthController@showRegistrationForm')->name('show.reg_form');
Route::post('register', 'Web\AuthController@register')->name('register');
Route::get('reset/password', 'Web\AuthController@show_password_reset_form')->name('get.password.reset');
Route::post('change/password', 'Web\AuthController@change_password')->name('change.password');
Route::post('reset/password/send', 'Web\AuthController@send_password_reset_link')->name('send.password.reset_link');
Route::get('forgot/passwd', 'Web\AuthController@show_forgot_password_form')->name('get.forgot.password');

// Social Login
Route::get('login/{provider}', 'Web\AuthController@redirectToProvider');
Route::get('login/{provider}/callback', 'Web\AuthController@handleProviderCallback');

// Authenticated pages
Route::group(['middleware' => 'custom_auth'], function () {
    
    Route::get('about', 'Web\PagesController@about_us_page')->name('about');
    Route::post('logout', 'Web\AuthController@logout')->name('logout');
    Route::get('dashboard', ['uses' => 'Web\UserController@dashboard', 'as' => 'dashboard']);
    Route::post('join-waiting-list', ['uses' => 'Web\UserController@join_waiting_list', 'as' => 'join-waiting-list']);
});
