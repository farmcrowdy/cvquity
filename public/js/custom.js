function joinWaitingList()
{
    $.confirm({
        useBootstrap: false,
        title: 'Confirm!',
        content: 'Click Confirm to join waiting list',
        buttons: {
            confirm: function () {
                var headers =  { Authorization: `Bearer ${token}` };
                $.ajax({
                    type: "POST",
                    url: endpoint+"join-waiting-list",
                    headers: headers,
                    dataType: "json",
                })
                .done(function(res) {
                    if(res.success)
                    {
                        toastr.success(res.message);
                    }
                    else {
                        toastr.error(res.message);
                    }
                    })
                .fail(function (jqXHR) {
                        toastr.error(jqXHR.responseJSON.message);
                    });
            },
            cancel: function () {
                $.alert('Canceled!');
            },
        }
    });
}