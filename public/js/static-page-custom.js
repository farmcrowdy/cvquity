// $(document).ready(function() {
  
//   // Logic that controls the input for amount and amount slider
//   $(document).on('input', '#amount, .amount_range', function() {
//     $('#amount').val($(this).val());
//     var getAmount = $('#amount').val()
//     var amount = Number(getAmount);
//     var amount_range = $('.amount_range').val($(this).val());
//     var getTenure = $('#tenure').val()
//     getTenure = parseFloat(getTenure)
//     let roi = $('#roi').val()
//     // console.log(roi);
//       let getInterest = Number(roi/100) * (amount);
//       let interest = getInterest + amount;
//       let formatInterest = Number(interest).toLocaleString();
//       $('#expectedAmount').text('₦'+formatInterest)
//   }) 

//   $.ajax({
//     method: "GET",
//     url: endpoint+"/api/bands",
//     })
//     .done(function( data ) {
//       data.data.forEach(element => {
//         $(document).on('input', '.tenure_range, #tenure', function() {
//           $('#tenure').val($(this).val());
//           $('.tenure_range').val($(this).val());
//           // console.log(data.data);
//           var getTenure = $('#tenure').val()
//           var tenureValue = parseFloat(getTenure)

//           // update the values accordingly
//           if(tenureValue <= element.maxTenor && tenureValue >= element.minTenor) {
//             // update the value of the hidden roi on the index page
//             let roi = $('#roi').val(element.roi);
//             updateValues(element.roi);  
//           }
//       })
//     });
//   }) 
  

// })

// function updateValues(calculatedRoi)
//   {
//     var getAmount = $('#amount').val()
//     var amount = Number(getAmount);
//     let getInterest = Number(calculatedRoi/100) * (amount);
//     let interest = getInterest + amount;
//     // console.log(interest);
//     let formatInterest = Number(interest).toLocaleString();
//     $('#expectedAmount').text('₦'+formatInterest)
//   }

// // range slider
// const slider = document.querySelector('.slider_container');
// if(slider){
//   $( '.slider_container input' ).on( 'input', function( ) {
//     //when the min value is greater than zero
//     let newMax_1 = (this.max/1) - (this.min/1);
//     let inputVal = (this.value/1) - (this.min/1)
//     let  val_1 =(inputVal/newMax_1)*100;
//      //when the min value is zero
//      let  val_2 =(this.value/this.max)*100;
//     if(this.min !== 0){
//       $( this ).css( 'background', 'linear-gradient(to right, #C0D271 0%, #C0D271 '+ (val_1 ) +'%, #D8D8D8 ' + (val_1)  + '%, #D8D8D8 100%)' );
//     }else{
//       $( this ).css( 'background', 'linear-gradient(to right, #C0D271 0%, #C0D271 '+ (val_2) +'%, #D8D8D8 ' + (val_2)  + '%, #D8D8D8 100%)' );
//     }
//     // console.log((this.value/this.max)*100);
//   });

// }

//  toggle password visibility
let input_field = $('#password_input') ;
let password_input = $('.password');
let input_span = $('.span_absolute') ;
let input_span_confirm = $('.span_absolute_confirm') ;
$(document).ready(()=>{
  input_span.click(()=>{
    if(input_field.val() != ""){
      if(input_span.hasClass('hidden_span')){
        input_span.removeClass('hidden_span');
        input_field.attr('type', 'text')
      }else{
        input_field.attr('type', 'password')
        input_span.addClass('hidden_span');
  
      }
    }
  })
});

// toggle password confirmation visibility
$(document).ready(()=>{
  input_span_confirm.click(()=>{
    if(password_input.val() != ""){
      if(input_span_confirm.hasClass('hidden_span_confirm')){
        input_span_confirm.removeClass('hidden_span_confirm');
        password_input.attr('type', 'text')
      }else{
        password_input.attr('type', 'password')
        input_span_confirm.addClass('hidden_span_confirm');
  
      }
    }
  })

  
});

// scrollspy Functions
const scrollspyer = document.querySelector('.about_body_container');
if(scrollspyer){
  function getSections($links) {
    return $(
      $links
        .map((i, el) => $(el).attr('href'))
        .toArray()
        .filter(href => href.charAt(0) === '#')
        .join(','),
  
    );
  }
  
  function activateLink($sections, $links) {
    const yPosition = $window.scrollTop() + 110;
    // console.log(yPosition);
  
    for (let i = $sections.length - 1; i >= 0; i -= 1) {
      const $section = $sections.eq(i);
  
      if (yPosition >= $section.offset().top) {
        return $links
          .removeClass('active')
          .filter(`[href="#${$section.attr('id')}"]`)
          .addClass('active');
      }
    }
  }
  
  function onScrollHandler() {
    activateLink($sections, $links);
  }
  
  function onClickHandler(e) {
    const href = $.attr(e.target, 'href');
  
    e.preventDefault();
    $root.animate(
      { scrollTop: $(href).offset().top },
      500,
      () => (window.location.hash = href),
    );
  
    return false;
  }
  
  // Variables
  const $window = $(window);
  const $links = $('.menu_spy > li > a');
  const $sections = getSections($links);
  const $root = $('html, body');
  const $hashLinks = $('a[href^="#"]:not([href="#"])');
  
  // Events
  $window.on('scroll', onScrollHandler);
  $hashLinks.on('click', onClickHandler);
  
  // Body
  activateLink($sections, $links);
  
}

// $(function(){
  const sponsorship_chech = $(".sponsorship_check")

  // toggle name edit on sponsorship savings 
  if(sponsorship_chech){
    // $(".view_detail").click((e, index)=>{
    //   $(e.target).fadeOut();
    //   console.log(index);
    // })
    let allContainers = document.querySelectorAll(".product_body");
    $(allContainers).click(function(){
       $(this).find("div.view_detail").fadeOut(1000);
       $(this).find("div.hidden_element").fadeIn(500);
    })
  }
//view details of sponsorships
if(sponsorship_chech){
  $(".toggler").click((e)=>{
    $(e.target).parent(".toggle_name").toggleClass("toggle_name_class");
  })
}
  // Remove svg.radial-progress .complete inline styling
  if(sponsorship_chech){
    $('svg.radial-progress').each(function( index, value ) { 
      $(this).find($('circle.complete')).removeAttr( 'style' );
  });

  // Activate progress animation on scroll
  $(window).scroll(function(){
      $('svg.radial-progress').each(function( index, value ) { 
          // If svg.radial-progress is approximately 25% vertically into the window when scrolling from the top or the bottom
          if ( 
              $(window).scrollTop() > $(this).offset().top - ($(window).height() * 0.75) &&
              $(window).scrollTop() < $(this).offset().top + $(this).height() - ($(window).height() * 0.25)
          ) {
              // Get percentage of progress
              percent = $(value).data('percentage');
              // Get radius of the svg's circle.complete
              radius = $(this).find($('circle.complete')).attr('r');
              // Get circumference (2πr)
              circumference = 2 * Math.PI * radius;
              // Get stroke-dashoffset value based on the percentage of the circumference
              strokeDashOffset = circumference - ((percent * circumference) / 100);
              // Transition progress for 1.25 seconds
              $(this).find($('circle.complete')).animate({'stroke-dashoffset': strokeDashOffset}, 1250);
          }
      });
  }).trigger('scroll');
  }
  

// });
// profile menu on mobile
$("#burger").click(()=> {
  $("#add_class_on_mobile").removeClass("hide_class");
  $(".dash_overlay").addClass("active");
  $("#add_class_on_mobile").addClass("slideInLeft");
  $("#activate").hide();

});
$(".dash_overlay").click(()=> {
  $("#add_class_on_mobile").removeClass("slideInLeft");
  $(".dash_overlay").removeClass("active");
  $("#add_class_on_mobile").addClass("slideInRight ");
  $("#activate").show();
  setTimeout(()=> {
    $("#add_class_on_mobile").removeClass("slideInLeft ");
  }, 600);
});

// transaction history chart 
let callChart = document.querySelector("#transaction");

if(callChart){
  var options = {
    series: [4, 1, 6],
    labels: ["Wallet Top-ups", "Payouts", "Withdrawals"],
    colors:['#54AB68', '#0077B5', '#D8602C'],
    chart: {
    type: 'donut',
    width: 180
  
  },
  legend: {
    show:false,
  },
  dataLabels: {
    enabled: false,
  },
  };
  
  var chart = new ApexCharts(callChart, options);
  chart.render();
}

 // switch tab to select
 const changeTab = document.getElementById('mySelect');
 if(changeTab){
  $(changeTab).on('change', function (e) {
    $('#nav-tab a').eq($(this).val()).tab('show');
  });
 }

//  toggle vissibility of amount on dashboard
const allContainers = document.querySelectorAll('.bal_cont');
if(allContainers){
  $('.toggle_bal_btn').click(()=>{
    $(allContainers).toggleClass("hide_balance");
  })
}
//copy content to clipboard
const copyClip = ()=> {
let copyText = document.getElementById("my_Input");
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  document.execCommand("copy");
  // 
}
const copyBtn = document.getElementById("copy_btn");
if(copyBtn){
  $(copyBtn).click(function(){
    copyClip();
  });
}

//display selected input file in dashboard profile
// get list of all the labels with the file input type 
let inputFiles = document.querySelectorAll(".file_label");
if(inputFiles){
  for (let element of inputFiles) {
    // console.log(element);
    // for a particular selected input 
      element.onchange = (e)=> { 
        let parentContainer = element.children;
        // get the value of the input 
        let fileName = parentContainer[0].value;
        // replace the span with the value of the input 
        parentContainer[1].innerHTML = fileName;
        // add a class to the label 
        element.classList.add('file_added');
    }
  }
}
// upload file and display it(contact person)
        // get list of all the input containers 
let inputContainers = document.querySelectorAll(".upload_and_display_img");
if(inputContainers){
  for (let input of inputContainers) {
      // for the selected input 
      input.onchange = (e)=> { 
        let mainContainer = input.children;
          // get the image in the container 
        let containerImage = mainContainer[1];
        // assign the selected src to it (for it to display)
        containerImage.src = URL.createObjectURL(e.target.files[0])

      }
  }
}

// Get the modal
var modal = document.getElementById("myModal");
// update modal 
var modalAdd = document.getElementById("myModal_add");

// Get all links that opens the modal
let openModalBtn = document.querySelectorAll(".open_modal");
let openModalAdd = document.querySelector(".open_add_modal");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close_modal")[0];
var span1 = document.getElementsByClassName("close_modal")[1];
if(modalAdd){
    // load add modal 
    openModalAdd.onclick = ()=> {
      modalAdd.style.display = "block";
    }
     // When the user clicks on <span> (x), close the modal
     span1.onclick = ()=> {
      modalAdd.style.display = "none";
    }
}
if(modal){
  for (const element of openModalBtn) {
        // When a user's name is clicked, open the modal 
      element.onclick = ()=> {
        modal.style.display = "block";
    }
  }
    // When the user clicks on <span> (x), close the modal
    span.onclick = ()=> {
      modal.style.display = "none";
    }
  
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = (event)=> {
      if (event.target == modal) {
        modal.style.display = "none";
      }
      if (event.target == modalAdd) {
        modalAdd.style.display = "none";
      }
    }
}

//checkout modal
// Get the modal
var checkoutModal = document.querySelectorAll(".checkout_modal");

// Get all links that opens the modal
let fireModalBtn = document.querySelectorAll(".checkout_container");
if(checkoutModal){
  for (const element of fireModalBtn) {
        // When a user clicks submit, open the modal 
      let clickedElement =  element.children[1];
      element.onclick = ()=> {
        clickedElement.style.display = "block";
        // clickedElement.classList.add("active_checkout")
        // close modal when widow is clicked 
        window.onclick = (event)=> {
          if (event.target == clickedElement) {
            clickedElement.style.display = "none";
          }
        }
        let closeIcon = clickedElement.children[0].children[0];
        closeIcon.onclick = ()=> {
          clickedElement.style.display = "none";
        }
      }
  }

}


var required = $('input,textarea,select').filter('[required]:visible');
var allRequired = true;
required.each(function(){
    if($(this).val() == ''){
        allRequired = false;
    }
});

if(!allRequired){
    //DO SOMETHING HERE... POPUP AN ERROR MESSAGE, ALERT , ETC.
}

// SVG animation on about page
const durationSlider = ()=> {
  var listItems = $('.animate_svg_logo .animate_logo_container path').length;
  var count = 0;
   
  setInterval(()=> {
    $('.animate_svg_logo .animate_logo_container path.animated').removeClass('animated');
    $('.animate_svg_logo .animate_logo_container path:eq(' + count +   ')').addClass('animated');	

    count += 1;
    if (count >= listItems) {
      count = 0;
    }
  }, 3000);

}
durationSlider();

// dashboard carousel
// let dash_carousel = document.getElementById('testimonial_carousel');
// if(dash_carousel){
// $('.testimonial_carousel_content').owlCarousel({
//   stagePadding: 35,
//   loop: true,
//   margin: 45,
//   dots: false,
//   autoplay: true,
//   autoplayTimeout: 5000,
//   autoplayHoverPause: true,
//   responsiveClass: true,
//   responsive: {
//     0: {
//       items: 1,
//       nav: false
//     },
//     375: {
//       items: 1,
//       nav: true,
//       stagePadding: 45
//     },
//     600: {
//       items: 2,
//       nav: true,
//       margin: 25,
//     },
//     840: {
//       items: 3,
//       nav: true,
//       margin: 20,
//     },
//     1200: {
//       stagePadding: 40,
//       items: 4,
//       margin: 20,

//     },

//     1400: {
//       items: 4,
//       margin: 40,
//     },
//     1430: {
//       items: 4,
//       margin: 45,
//       stagePadding: 80,

//     },
//     1800: {
//       stagePadding: 100,
//       items: 4,
//       margin: 30,

//     },
//     2400: {
//       stagePadding: 100,
//       items: 5,
//       margin: 30,

//     }
//   }
// })
// }