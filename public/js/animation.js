
  // navbar and hero animation 
  let navHead = document.getElementById("header_sticky");
  let tl1 = gsap.timeline();
if(navHead){

  tl1.from(".logo", 1, {
      opacity: 0,
      x: -20,
      ease: Power3.easeInOut,
    })
  tl1.from(".nav_ul li", 1, {
      opacity: 0,
      x: -20,
      ease: Power2.easeOut,
      stagger: 0.08
    },"-=0.75");
    // index hero section 
    let indexHero = document.querySelector(".index_hero");
    if(indexHero){
      tl1.from(".hero_content",{
        opacity: 0,
        y: 10,
        ease: Power2.easeOut,
      },'-=0.8')
      tl1.from(".movable_bg",{
        scale: 0.1, 
        opacity:0.5,
        duration:20,
        ease: Power2.easeOut,
      },'-=1.6')
      tl1.to(".movable_bg",{
        scale: 0.9, 
        opacity:0.8,
        duration:5,
        stagger: 0.08,
        delay:5,
        ease: Power2.easeOut,
        yoyo: true, 
        repeat: 5, 
      });
    }
}
     //  hero section for other static pages 
     let AltHero = document.querySelector(".option_home");
     if(AltHero){
        tl1.from(".hero_content .option_title_container", 2, {
          opacity: 0,
          y: 10,
          ease: Power2.easeOut,
        })
        .from(".option_img_container", 3, {
          opacity: 0,
          scale:"0.3",
          ease: Power2.easeOut,
        },'-=1.6')
      }
          //  hero section for other static pages 
     let AbtHero = document.querySelector(".about_svg");
     if(AbtHero){
      tl1.from(".about_svg svg", 2, { opacity: 0, y: 10, ease: Power2.easeOut}, 0)
      .from(".about_svg h3", 2, { opacity: 0, y: 10, ease: Power2.easeOut}, "-=1.5")
      .from(".about_svg svg .human1", 5, { opacity: 0, y: 70, ease: Power2.easeOut}, "+=1")
      .from(".about_svg svg .e_human", 7, { opacity: 0.4, y: 70, ease: Power2.easeOut}, "+=1")
      .from(".about_svg svg .human2", 6, { opacity: 0.4, y: 90, ease: Power2.easeOut}, "+=1");
      }
         //  auth animation
     let authContainer = document.querySelector(".auth_container");
     if(authContainer){
      tl1.from(authContainer, 1.5, { opacity: 0, y: 10, ease: Power2.easeOut});

     }
    /*scroll magic*/
    let ctrl = new ScrollMagic.Controller();
  // Create scenes in jQuery each() loop
  let scrollAnimation = document.querySelector('.animation_main');
  if(scrollAnimation){
    $('.animation_main').each(function(e) {
      let whySection = $(this).find(".why_item");
      let whySvg = $(this).find(".why_item .svg_container");
      let growIcon = $(this).find(".product_body svg");
      let growText1 = $(this).find(".product_body h4");
      let growText2 = $(this).find(".product_body p");
      let pilotSvg = $(this).find(".autopilot_item ._svg");
      let pilotItem = $(this).find(".autopilot_item .body_item");
      let team = $(this).find(".img_container img");
      let teamText = $(this).find(".body_bg .how_simple p");
      let appImg = $(this).find(".mobile_app img");
      // sponsorhip home page 
      let sponsorshipOption = $(this).find(".how_it_works_card_item ");
      let sponsorshipOptionH4 = $(this).find(".how_it_works_card_item h4");
      let sponsorshipOptionP = $(this).find(".how_it_works_card_item p");
      let guide = $(this).find(".guide_item");
      // about home page 
      let ourImg = $(this).find(".our_img");
      let missionCard = $(this).find(".card_item");
      let missionCardSvg = $(this).find(".card_item .svg_container");
      let missionCardP = $(this).find(".card_item p");
      let joinTeam = $(this).find(".join_svg");

    
      let tl2 = gsap.timeline();
      gsap.config({
        nullTargetWarn: false,
      });
      tl2.fromTo(whySection, 1, {y:"20%",
       opacity:"0.7",
      }, { y:"0%",
          opacity:"1", 
          stagger: 0.08,
          duration:1,
          ease: Power2.easInOut,},0)
          .fromTo(whySvg, 2, { scale:"0.7", opacity:"0.4"}, {scale:"1", opacity:"1",  ease: Power2.easInOut},'-=0.5');
  
          // grow yor money section 
          tl2.fromTo(growText1, 1, { y:"20%", opacity:"0.4"}, {y:"0%", opacity:"1",  ease: Power2.easInOut}, 0)
          .fromTo(growText2, 1, { y:"20%", opacity:"0.6"}, {y:"0%", opacity:"1",  ease: Power2.easInOut},0)
        .fromTo(growIcon, 2, { scale:"0.7", opacity:"0.4"}, {scale:"1", opacity:"1",  ease: Power2.easInOut},'-=0.5');
        //autopilot section
        tl2.fromTo(pilotItem, 2, { opacity:"0.3", y:"20%", stagger: 0.08}, { opacity:"1", y:"0%",stagger: 0.08, ease: Power2.easInOut}, 0)
        .fromTo(pilotSvg, 2, { scale:"0.3", x:"20%", transformOrigin: "cemter center", rotate:90, opacity:"0.4"}, {scale:"1", opacity:"1",transformOrigin: "cemter center", rotate:0, x:"0",  ease: Power2.easInOut}, 0);
        //team section
        tl2.fromTo(teamText, 1, { scale:"0.9", opacity:"0.3"}, {scale:"1", opacity:"1", ease: Power2.easInOut}, 0)
        .fromTo(team, 1.5, { opacity:"0.3"}, { opacity:"1", ease: Power2.easInOut}, 0);
        //app section
        tl2.fromTo(appImg, 1, { scale:"0.8",y:"15%", opacity:"0"}, {scale:"1", y:"0%", opacity:"1", ease: Power2.easInOut}, 0);
            // sponsorhip home page 
        tl2.fromTo(sponsorshipOption, 2, { y:"20%"}, { y:"0%", ease: Power2.easInOut}, 0)
        .fromTo(sponsorshipOptionH4, 2, {opacity: "0.6", y:"10%"}, {opacity: "1", y:"0%", ease: Power2.easInOut},0)
        .fromTo(sponsorshipOptionP, 2, {opacity: "0.6", y:"10%"}, {opacity: "1",  y:"0%", ease: Power2.easInOut},0);
        tl2.fromTo(guide, 2, { background:"#F4EFDB", stagger:0.8,  y:"20%"}, { background:"#EFEFEF", y:"0%", ease: Power2.easInOut}, 0);
        // about home page 
        tl2.fromTo(ourImg, 2, { filter: "blur(2px)", y:"10%"}, { filter: "blur(0px)", y:"0%", ease: Power2.easInOut}, 0);
        tl2.fromTo(missionCard, 2, { background:"#F4EFDB" , y:"20%"}, {   background:"#ffffff", y:"0%", ease: Power2.easInOut}, 0)
        .fromTo(missionCardSvg, 2, { scale:"0.3" }, {   scale:"1", ease: Power2.easInOut}, 0)
        .fromTo(missionCardP, 2, { opacity:"0.5" }, {   opacity:"1", ease: Power2.easInOut}, 0);
        tl2.fromTo(joinTeam, 2, { scale:"0.5", opacity:0.4 }, { opacity:"1",  scale:"1", ease: Power2.easInOut}, 0);

      new ScrollMagic.Scene({
        triggerElement: this,
        duration: 0,
        triggerHook: 0.8,
        offset: 1
      })
      
        .setTween(tl2)
        .addTo(ctrl);
        // Scene.addIndicators();
    });
  }
  
  